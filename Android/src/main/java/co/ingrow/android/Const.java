package co.ingrow.android;

public class Const {

    public static final String PROJECT = "project";
    public static final String STREAM = "stream";
    public static final String INGROW = "ingrow";
    public static final String EVENT = "event";
    public static final String ENRICHMENT = "enrichment";
    public static final String ANONYMOUS_ID = "anonymous_id";
    public static final String USER_ID = "user_id";
    public static final String SESSION_ID = "session_id";
    public static final String NAME = "name";
    public static final String SESSION = "session";
    public static final String INPUT = "input";
    public static final String API_KEY = "api-key";
}
