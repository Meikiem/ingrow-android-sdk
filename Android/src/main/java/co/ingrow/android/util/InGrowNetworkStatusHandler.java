package co.ingrow.android.util;

import android.app.Application;

public interface InGrowNetworkStatusHandler {
    boolean isNetworkConnected();
}
